package constance;

import model.Student;

import java.util.ArrayList;
import java.util.List;

public class CollectionStudents {

    //            - список студентов заданного факультета;
//    - списки студентов для каждого факультета и курса;
//    - список студентов, родившихся после заданного года;
//    - студента, родившихся после заданного года;
//    - список учебной группы в формате Фамилия, Имя, Отчество (List<String>).


    private static final String address = "Hogwarts";
    public static final String SLYTHERIN = "Slytherin";
    public static final String GRYFFINDOR = "Gryffindor";
    public static final String RAVENCLAW = "Ravenclaw";
    public static final String HUFFLEPUFF = "Hufflepuff";

    private static final Student student1 = new Student(1, "Cedric", "Diggory", 1977, address, "077077898", HUFFLEPUFF, "4", "3B");
    private static final Student student2 = new Student(2, "Harry", "Potter", 1979, address, "0666669999", GRYFFINDOR, "2", "2A");
    private static final Student student3 = new Student(3, "Hermione", "Granger", 1979, address, "0770332233", GRYFFINDOR, "2", "2A");
    private static final Student student4 = new Student(4, "Draco", "Malfoy", 1979, address, "077077897", SLYTHERIN, "2", "2A");
    private static final Student student5 = new Student(5, "Lucius", "Malfoy", 1960, address, "0888888666", SLYTHERIN, "5", "1B");
    private static final Student student6 = new Student(6, "Luna", "Lovegood", 1980, address, "0888888888", RAVENCLAW, "1", "4C");
    private static final Student student7 = new Student(7, "Severus", "Snape", 1960, address, "01112223344", SLYTHERIN, "5", "1B");

    public static final List<Student> STUDENTS = new ArrayList<>(List.of(student1, student2, student3, student4, student5, student6, student7));

}
