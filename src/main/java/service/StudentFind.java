package service;

import model.Student;

import java.util.List;

public class StudentFind {
    //6) С помощью find:
//            - подсчитать количество студентов на заданном факультете.


    public int findOnFacultyCount(List<Student> students, String faculty){

        if (students == null){
            return 0;
        }

        int countStudents = (int) students.stream().filter(student -> student.getFaculty().equals(faculty)).count();

        return countStudents;
    }



}
