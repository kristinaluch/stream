package service;

import model.Student;

import java.util.List;

public class StudentsCheck {

    public boolean checkAllStudyFaculty(List<Student> students, String faculty){
        if(students == null||students.size() == 0) {
            return false;
        }
        boolean allStudyInFaculty = students.stream().allMatch(student -> student.getFaculty().equals(faculty));

        return allStudyInFaculty;
    }

    public boolean checkAnyStudyFaculty(List<Student> students, String faculty){
        if(students == null||students.size() == 0) {
            return false;
        }
        boolean anyStudyInFaculty = students.stream().anyMatch(student -> student.getFaculty().equals(faculty));
        return anyStudyInFaculty;
    }

    public boolean checkAllStudyCourse(List<Student> students, String course){
        if(students == null||students.size() == 0) {
            return false;
        }

        boolean allStudyInGroup = students.stream().allMatch(student -> student.getCourse().equals(course));

        return allStudyInGroup;
    }

    public boolean checkAnyStudyCourse(List<Student> students, String course){
        if(students == null||students.size() == 0) {
            return false;
        }
        boolean anyStudyInGroup = students.stream().anyMatch(student -> student.getCourse().equals(course));

        return anyStudyInGroup;
    }

    public boolean checkAllStudyGroup(List<Student> students, String group){
        if(students == null||students.size() == 0) {
            return false;
        }
        boolean allStudyInGroup = students.stream().allMatch(student -> student.getGroup().equals(group));

        return allStudyInGroup;
    }

    public boolean checkAnyStudyGroup(List<Student> students, String group){
        if(students == null||students.size() == 0) {
            return false;
        }
        boolean anyStudyInGroup = students.stream().anyMatch(student -> student.getGroup().equals(group));

        return anyStudyInGroup;
    }




}
