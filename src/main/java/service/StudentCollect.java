package service;

import model.Student;

import java.util.List;
import java.util.stream.Collectors;

public class StudentCollect {

    //3)
//  1.          - список студентов заданного факультета;
    public List<Student> getStudentsFaculty(List<Student> students, String faculty) {

        if (students == null) {
            return null;
        }

        List<Student> studentsInFaculty = students.stream()
                .filter(student -> student.getFaculty().equals(faculty))
                .collect(Collectors.toList());

        return studentsInFaculty;
    }

// 2.   - списки студентов для каждого факультета и курса;

    public List<Student> getStudentsOnCourse(List<Student> students, String faculty, String course) {
        if (students == null) {
            return null;
        }
        List<Student> studentsOnCourse = students.stream()
                .filter(student -> student.getFaculty().equals(faculty))
                .filter(student -> student.getCourse().equals(course))
                .collect(Collectors.toList());
        return studentsOnCourse;
    }

    //  3.      - список студентов, родившихся после заданного года;
    public List<Student> getStudentsYear(List<Student> students, int year) {
        if (students == null) {
            return null;
        }
        List<Student> studentsBirthOnYear = students.stream()
                .filter(student -> student.getYearOfBirth() > year)
                .collect(Collectors.toList());

        return studentsBirthOnYear;
    }

    // 4.     - студента, родившихся после заданного года;
    public Student getOneStudentYear(List<Student> students, int year) {
        if (students == null) {
            return null;
        }
        Student studentInFaculty = students.stream()
                .filter(student -> student.getYearOfBirth() > year).findFirst().get();

        return studentInFaculty;
    }

    // 5.    - список учебной группы в формате Фамилия, Имя, Отчество (List<String>).
    public List<String> getStudentsGroupList(List<Student> students, String group) {
        if (students == null) {
            return null;
        }
        List<String> studentsList = students.stream()
                .filter(student -> student.getGroup().equals(group))
                .map(student -> student.getFirstName() + " " + student.getLastName())
                .collect(Collectors.toList());
        return studentsList;
    }

}

