package service;

import model.Student;

import java.util.List;

public class StudentForm {

    //            7) С помощью reduce:
//            - вывести строку в формате Фамилия, Имя, Отчество - Факультет, Группа;
//    разделитель перенос строки, и принимает ограничение по кол-ву студентов;


    private static final String COMMA_WITH_SPACE = ", ";
    private static final String ENTER = "\n";

    public String getFormat(List<Student> students, int limit) {

        if (students == null || limit <= 0){
            return null;
        }

        String studentString = students.stream()
                .limit(limit)
                .reduce("", (partialFirstNameResult, student) -> partialFirstNameResult + toForm(student) + ENTER, String::concat);
        return studentString;
    }

    private String toForm(Student student){
        String form = student.getFirstName() + COMMA_WITH_SPACE
                +student.getLastName() + " - "
                +student.getFaculty() + COMMA_WITH_SPACE
                +student.getGroup() + ";";
        return form;
    }


}
