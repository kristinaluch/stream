package service;

import model.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StudentsChange {

    public List<Student> changeFacultyStudent(List<Student> students, String facultyFrom, String facultyTo){

        if (students == null) {
            return null;
        }

        if (facultyFrom==null || facultyFrom.isBlank() || facultyTo==null || facultyTo.isBlank()){
            return new ArrayList<Student>();
        }

        List<Student> studentsChangeFaculty = students.stream()
                .filter(student -> student.getFaculty().equals(facultyFrom))
                .map(student -> getChangeFaculty(student, facultyTo))
                .collect(Collectors.toList());
        return studentsChangeFaculty;
    }

    private Student getChangeFaculty(Student student, String faculty){
        student.setFaculty(faculty);
        return student;
    }

    public List<Student>  changeGroupStudent(List<Student> students, String groupFrom, String groupTo){

        if (students == null) {
            return null;
        }

        if (groupFrom == null || groupFrom.isBlank() || groupTo == null || groupTo.isBlank()){
            return new ArrayList<Student>();
        }

        List<Student>  studentsChangeGroup = students.stream()
                .filter(student -> student.getGroup().equals(groupFrom))
                .map(student -> getChangeGroup(student, groupTo))
                .collect(Collectors.toList());
        return studentsChangeGroup;
    }

    private Student getChangeGroup(Student student, String group){
        student.setGroup(group);
        return student;
    }



}
