package service;

import model.Student;

import java.util.List;

public class StudentsCount {

    //    4) С помощью count:
//            - подсчитать количество студентов на заданном факультете.
    public int getCountOnFaculty(List<Student> students, String faculty) {
        if (students == null) {
            return 0;
        }
        int studentsOnFaculty = (int) students.stream()
                .filter(student -> student.getFaculty().equals(faculty))
                .count();
        return studentsOnFaculty;
    }
}
