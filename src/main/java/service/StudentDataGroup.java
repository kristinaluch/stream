package service;

import model.Student;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StudentDataGroup {

    public Map<String, List<Student>>  getMapFaculty(List<Student> students){
        Map<String, List<Student>> studentsInFaculty = students.stream()
                .collect(Collectors.groupingBy(Student::getFaculty));
        return studentsInFaculty;
    }

    public Map<String, List<Student>>  getMapCourse(List<Student> students){
        Map<String, List<Student>> studentsInCourse = students.stream()
                .collect(Collectors.groupingBy(Student::getCourse));
        return studentsInCourse;
    }

    public Map<String, List<Student>>  getMapGroup(List<Student> students){
        Map<String, List<Student>> studentsInGroup = students.stream()
                .collect(Collectors.groupingBy(Student::getGroup));
        return studentsInGroup;
    }

}
