package testdata.service;


import model.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StudentDataGroupTestData extends StudentTestData{

    public static final Map<String, List<Student>> STUDENTS_IN_FACULTY = Map.of(HUFFLEPUFF, List.of(STUDENT_1),
            GRYFFINDOR, List.of(STUDENT_2, STUDENT_3),
            SLYTHERIN, List.of(STUDENT_4, STUDENT_5, STUDENT_7),
            RAVENCLAW, List.of(STUDENT_6));

    public static final Map<String, List<Student>> STUDENTS_IN_COURSE = Map.of("4", List.of(STUDENT_1),
            "2", List.of(STUDENT_2, STUDENT_3, STUDENT_4),
            "5", List.of(STUDENT_5, STUDENT_7),
            "1", List.of(STUDENT_6));

    public static final Map<String, List<Student>> STUDENTS_IN_GROUP = Map.of("3B", List.of(STUDENT_1),
        "2A", List.of(STUDENT_2, STUDENT_3, STUDENT_4),
        "1B", List.of( STUDENT_5, STUDENT_7),
        "4C", List.of(STUDENT_6));

}
