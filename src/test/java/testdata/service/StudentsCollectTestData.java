package testdata.service;

import model.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentsCollectTestData extends StudentTestData {

    public static final List<Student> STUDENTS_IN_GRYFFINDOR = new ArrayList<Student>(List.of(STUDENT_2, STUDENT_3));
    public static final List<Student> STUDENTS_IN_SLYTHERIN = new ArrayList<Student>(List.of(STUDENT_4, STUDENT_5, STUDENT_7));

    public static final List<Student> STUDENTS_GRYFFINDOR_2 = new ArrayList<Student>(List.of(STUDENT_2, STUDENT_3));
    public static final List<Student> STUDENTS_SLYTHERIN_2 = new ArrayList<Student>(List.of(STUDENT_4));
    public static final List<Student> STUDENTS_AFTER_1970 = new ArrayList<Student>(List.of(STUDENT_1, STUDENT_2, STUDENT_3, STUDENT_4, STUDENT_6));
    public static final List<String> STUDENTS_GROUP_1B_LIST = new ArrayList<String>(List.of("Lucius Malfoy", "Severus Snape"));

}
