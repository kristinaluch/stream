package testdata.service;

import model.Student;

import java.util.List;

public class StudentsCheckTestData extends StudentTestData{

    public static final List<Student> STUDENT_IN_FACULTY_ALL_SUCCESS = List.of(STUDENT_4, STUDENT_5, STUDENT_7);
    public static final List<Student> STUDENT_IN_FACULTY_ALL_WRONG_1 = List.of(STUDENT_4, STUDENT_2, STUDENT_7);
    public static final List<Student> STUDENT_IN_FACULTY_ALL_WRONG_2 = List.of(STUDENT_1, STUDENT_5, STUDENT_7);
    ;
    public static final List<Student> STUDENT_IN_FACULTY_ANY_SUCCESS = List.of(STUDENT_4, STUDENT_2, STUDENT_1);
    public static final List<Student> STUDENT_IN_FACULTY_ANY_WRONG_1 = List.of(STUDENT_3, STUDENT_1, STUDENT_2);
    public static final List<Student> STUDENT_IN_FACULTY_ANY_WRONG_2 = List.of(STUDENT_3, STUDENT_2, STUDENT_3, STUDENT_6);

    public static final List<Student> STUDENT_IN_COURSE_ALL_SUCCESS = List.of(STUDENT_2, STUDENT_3, STUDENT_4);
    public static final List<Student> STUDENT_IN_COURSE_ALL_WRONG_1 = List.of(STUDENT_4, STUDENT_5, STUDENT_3);
    public static final List<Student> STUDENT_IN_COURSE_ALL_WRONG_2 = List.of(STUDENT_6, STUDENT_4, STUDENT_5);

    public static final List<Student> STUDENT_IN_COURSE_ANY_SUCCESS = List.of(STUDENT_1, STUDENT_2, STUDENT_5);
    public static final List<Student> STUDENT_IN_COURSE_ANY_WRONG_1 = List.of(STUDENT_6, STUDENT_7, STUDENT_1);
    public static final List<Student> STUDENT_IN_COURSE_ANY_WRONG_2 = List.of(STUDENT_1, STUDENT_5, STUDENT_7);

    public static final List<Student> STUDENT_IN_GROUP_ALL_SUCCESS = List.of(STUDENT_2, STUDENT_3, STUDENT_4);
    public static final List<Student> STUDENT_IN_GROUP_ALL_WRONG_1 = List.of(STUDENT_4, STUDENT_5, STUDENT_3);
    public static final List<Student> STUDENT_IN_GROUP_ALL_WRONG_2 = List.of(STUDENT_6, STUDENT_4, STUDENT_5);

    public static final List<Student> STUDENT_IN_GROUP_ANY_SUCCESS =  List.of(STUDENT_1, STUDENT_2, STUDENT_5);
    public static final List<Student> STUDENT_IN_GROUP_ANY_WRONG_1 =  List.of(STUDENT_6, STUDENT_7, STUDENT_1);
    public static final List<Student> STUDENT_IN_GROUP_ANY_WRONG_2 =  List.of(STUDENT_1, STUDENT_5, STUDENT_7);



}
