package testdata.service;

public class StudentFormTestData extends StudentTestData{

    public static final String STUDENTS_FORM = "Cedric, Diggory - Hufflepuff, 3B;\n" +
            "Harry, Potter - Gryffindor, 2A;\n" +
            "Hermione, Granger - Gryffindor, 2A;\n" +
            "Draco, Malfoy - Slytherin, 2A;\n";

}
