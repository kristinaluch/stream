package testdata.service;

import model.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentsChangeTestData extends StudentTestData{

    public static final Student STUDENT_2_G = new Student(2, "Harry", "Potter", 1979, ADDRESS, "0666669999", GRYFFINDOR, "2", "2A");
    public static final Student STUDENT_3_G = new Student(3, "Hermione", "Granger", 1979, ADDRESS, "0770332233", GRYFFINDOR, "2", "2A");

    public static final Student STUDENT_2_TO_SLYTHERIN = new Student(2, "Harry", "Potter", 1979, ADDRESS, "0666669999", SLYTHERIN, "2", "2A");
    public static final Student STUDENT_3_TO_SLYTHERIN = new Student(3, "Hermione", "Granger", 1979, ADDRESS, "0770332233", SLYTHERIN, "2", "2A");

    public static final Student STUDENT_2_FROM_2A = new Student(2, "Harry", "Potter", 1979, ADDRESS, "0666669999", GRYFFINDOR, "2", "2A");
    public static final Student STUDENT_3_FROM_2A = new Student(3, "Hermione", "Granger", 1979, ADDRESS, "0770332233", GRYFFINDOR, "2", "2A");
    public static final Student STUDENT_4_FROM_2A = new Student(4, "Draco", "Malfoy", 1979, ADDRESS, "077077897", SLYTHERIN, "2", "2A");

    public static final Student STUDENT_2_TO_3B = new Student(2, "Harry", "Potter", 1979, ADDRESS, "0666669999", GRYFFINDOR, "2", "3B");
    public static final Student STUDENT_3_TO_3B = new Student(3, "Hermione", "Granger", 1979, ADDRESS, "0770332233", GRYFFINDOR, "2", "3B");
    public static final Student STUDENT_4_TO_3B = new Student(4, "Draco", "Malfoy", 1979, ADDRESS, "077077897", SLYTHERIN, "2", "3B");

    public static final List<Student> STUDENTS_FOR_CHANGE_FACULTY = new ArrayList<>(List.of(STUDENT_1, STUDENT_2_G, STUDENT_3_G, STUDENT_4, STUDENT_5, STUDENT_6, STUDENT_7));
    public static final List<Student> STUDENTS_FOR_CHANGE_GROUP = new ArrayList<>(List.of(STUDENT_1, STUDENT_2_FROM_2A, STUDENT_3_FROM_2A, STUDENT_4_FROM_2A, STUDENT_5, STUDENT_6, STUDENT_7));
    public static final List<Student> STUDENTS_GRYFFINDOR_TO_SLYTHERIN = new ArrayList(List.of(STUDENT_2_TO_SLYTHERIN, STUDENT_3_TO_SLYTHERIN));
    public static final List<Student> STUDENTS_2A_TO_3B = new ArrayList(List.of(STUDENT_2_TO_3B, STUDENT_3_TO_3B, STUDENT_4_TO_3B));

}
