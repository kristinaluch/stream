package testdata.service;

import model.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentTestData {

    public static final String ADDRESS = "Hogwarts";
    public static final String SLYTHERIN = "Slytherin";
    public static final String GRYFFINDOR = "Gryffindor";
    public static final String RAVENCLAW = "Ravenclaw";
    public static final String HUFFLEPUFF = "Hufflepuff";

    public static final Student STUDENT_1 = new Student(1, "Cedric", "Diggory", 1977, ADDRESS,
            "077077898", HUFFLEPUFF, "4", "3B");
    public static final Student STUDENT_2 = new Student(2, "Harry", "Potter", 1979, ADDRESS,
            "0666669999", GRYFFINDOR, "2", "2A");
    public static final Student STUDENT_3 = new Student(3, "Hermione", "Granger", 1979, ADDRESS,
            "0770332233", GRYFFINDOR, "2", "2A");
    public static final Student STUDENT_4 = new Student(4, "Draco", "Malfoy", 1979, ADDRESS,
            "077077897", SLYTHERIN, "2", "2A");
    public static final Student STUDENT_5 = new Student(5, "Lucius", "Malfoy", 1960, ADDRESS,
            "0888888666", SLYTHERIN, "5", "1B");
    public static final Student STUDENT_6 = new Student(6, "Luna", "Lovegood", 1980, ADDRESS,
            "0888888888", RAVENCLAW, "1", "4C");
    public static final Student STUDENT_7 = new Student(7, "Severus", "Snape", 1960, ADDRESS,
            "01112223344", SLYTHERIN, "5", "1B");


    public static final List<Student> STUDENTS = new ArrayList<>(List.of(STUDENT_1, STUDENT_2, STUDENT_3, STUDENT_4, STUDENT_5, STUDENT_6, STUDENT_7));


}
