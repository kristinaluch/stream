package service;

import model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.List;
import java.util.Map;

import static testdata.service.StudentDataGroupTestData.*;

class StudentDataGroupTest {

    StudentDataGroup cut = new StudentDataGroup();

    static Arguments[] getMapFacultyTestArgs(){
        return new Arguments[]{
                Arguments.arguments(STUDENTS, STUDENTS_IN_FACULTY),
        };
    }

    @MethodSource("getMapFacultyTestArgs")
    @ParameterizedTest
    void getMapFacultyTest(List<Student> students, Map<String, List<Student>> expected){
        Map<String, List<Student>> actual = cut.getMapFaculty(students);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getMapCourseTestArgs(){
        return new Arguments[]{
                Arguments.arguments(STUDENTS, STUDENTS_IN_COURSE),
        };
    }

    @MethodSource("getMapCourseTestArgs")
    @ParameterizedTest
    void getMapCourseTest(List<Student> students, Map<String, List<Student>> expected){
        Map<String, List<Student>> actual = cut.getMapCourse(students);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getMapGroupTestArgs(){
        return new Arguments[]{
                Arguments.arguments(STUDENTS, STUDENTS_IN_GROUP),
        };
    }

    @MethodSource("getMapGroupTestArgs")
    @ParameterizedTest
    void getMapGroupTest(List<Student> students, Map<String, List<Student>> expected){
        Map<String, List<Student>> actual = cut.getMapGroup(students);
        Assertions.assertEquals(expected, actual);
    }

}