package service;

import model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.List;
import static constance.CollectionStudents.*;

public class StudentCountTest {

    StudentsCount cut = new StudentsCount();

    static Arguments[] getCountOnFacultyTestArgs() {
        return new Arguments[]{
                Arguments.arguments(STUDENTS, GRYFFINDOR, 2),
                Arguments.arguments(STUDENTS, SLYTHERIN, 3),
                Arguments.arguments(STUDENTS, null, 0),
                Arguments.arguments(STUDENTS, " ", 0),
                Arguments.arguments(STUDENTS, "Winx", 0),
                Arguments.arguments(null, "Winx", 0),
        };
    }

    @MethodSource("getCountOnFacultyTestArgs")
    @ParameterizedTest
    void getCountOnFacultyTest(List<Student> students, String faculty, int expected) {
        int actual = cut.getCountOnFaculty(students, faculty);
        Assertions.assertEquals(expected, actual);
    }

}
