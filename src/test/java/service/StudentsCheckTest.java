package service;

import model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;

import static testdata.service.StudentsCheckTestData.*;

class StudentsCheckTest {

    StudentsCheck cut = new StudentsCheck();

    static Arguments[] checkAllStudyFacultyTestArgs(){
        return new Arguments[]{
                Arguments.arguments(STUDENT_IN_FACULTY_ALL_SUCCESS, SLYTHERIN, true),
                Arguments.arguments(STUDENT_IN_FACULTY_ALL_WRONG_1, SLYTHERIN, false),
                Arguments.arguments(STUDENT_IN_FACULTY_ALL_WRONG_2, SLYTHERIN, false),
                Arguments.arguments(null, SLYTHERIN, false),
                Arguments.arguments(new ArrayList<>(), SLYTHERIN, false)
        };
    }

    @MethodSource("checkAllStudyFacultyTestArgs")
    @ParameterizedTest
    void checkAllStudyFacultyTest(List<Student> students, String faculty, boolean expected) {
        boolean actual = cut.checkAllStudyFaculty(students, faculty);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] checkAnyStudyFacultyTestArgs(){
        return new Arguments[]{

                Arguments.arguments(STUDENT_IN_FACULTY_ANY_SUCCESS, SLYTHERIN, true),
                Arguments.arguments(STUDENT_IN_FACULTY_ANY_WRONG_1, SLYTHERIN, false),
                Arguments.arguments(STUDENT_IN_FACULTY_ANY_WRONG_2, SLYTHERIN, false),
                Arguments.arguments(null, SLYTHERIN, false),
                Arguments.arguments(new ArrayList<>(), SLYTHERIN, false)
        };
    }

    @MethodSource("checkAnyStudyFacultyTestArgs")
    @ParameterizedTest
    void checkAnyStudyFacultyTest(List<Student> students, String faculty, boolean expected) {
        boolean actual = cut.checkAnyStudyFaculty(students, faculty);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] checkAllStudyCourseTestArgs(){
        return new Arguments[]{
                Arguments.arguments(STUDENT_IN_COURSE_ALL_SUCCESS, "2", true),
                Arguments.arguments(STUDENT_IN_COURSE_ALL_WRONG_1, "2", false),
                Arguments.arguments(STUDENT_IN_COURSE_ALL_WRONG_2, "2", false),
                Arguments.arguments(null, SLYTHERIN, false),
                Arguments.arguments(new ArrayList<>(), SLYTHERIN, false)
        };
    }

    @MethodSource("checkAllStudyCourseTestArgs")
    @ParameterizedTest
    void checkAllStudyCourseTest(List<Student> students, String faculty, boolean expected) {
        boolean actual = cut.checkAllStudyCourse(students, faculty);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] checkAnyStudyCourseTestArgs(){
        return new Arguments[]{
                Arguments.arguments(STUDENT_IN_COURSE_ANY_SUCCESS, "2", true),
                Arguments.arguments(STUDENT_IN_COURSE_ANY_WRONG_1, "2", false),
                Arguments.arguments(STUDENT_IN_COURSE_ANY_WRONG_2, "2", false),
                Arguments.arguments(null, "2", false),
                Arguments.arguments(new ArrayList<>(), "2", false)
        };
    }

    @MethodSource("checkAnyStudyCourseTestArgs")
    @ParameterizedTest
    void checkAnyStudyCourseTest(List<Student> students, String faculty, boolean expected) {
        boolean actual = cut.checkAnyStudyCourse(students, faculty);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] checkAllStudyGroupTestArgs(){
        return new Arguments[]{
                Arguments.arguments(STUDENT_IN_GROUP_ALL_SUCCESS, "2A", true),
                Arguments.arguments(STUDENT_IN_GROUP_ALL_WRONG_1, "2A", false),
                Arguments.arguments(STUDENT_IN_GROUP_ALL_WRONG_2, "2A", false),
                Arguments.arguments(null, "2A", false),
                Arguments.arguments(new ArrayList<>(), "2A", false)
        };
    }


    @MethodSource("checkAllStudyGroupTestArgs")
    @ParameterizedTest
    void checkAllStudyGroupTest(List<Student> students, String faculty, boolean expected) {
        boolean actual = cut.checkAllStudyGroup(students, faculty);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] checkAnyStudyGroupTestArgs(){
        return new Arguments[]{
                Arguments.arguments(STUDENT_IN_GROUP_ANY_SUCCESS, "2A", true),
                Arguments.arguments(STUDENT_IN_GROUP_ANY_WRONG_1, "2A", false),
                Arguments.arguments(STUDENT_IN_GROUP_ANY_WRONG_2, "2A", false),
                Arguments.arguments(null, "2A", false),
                Arguments.arguments(new ArrayList<>(), "2A", false) };
    }

    @MethodSource("checkAnyStudyGroupTestArgs")
    @ParameterizedTest
    void checkAnyStudyGroupTest(List<Student> students, String faculty, boolean expected) {
        boolean actual = cut.checkAnyStudyGroup(students, faculty);
        Assertions.assertEquals(expected, actual);
    }

}