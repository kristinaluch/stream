package service;

import model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.ArrayList;
import java.util.List;
import static testdata.service.StudentsCollectTestData.*;

class StudentCollectTest {

    StudentCollect cut = new StudentCollect();

    static Arguments[] getStudentsFacultyTestArgs(){
        return new Arguments[]{
                Arguments.arguments(STUDENTS, GRYFFINDOR, STUDENTS_IN_GRYFFINDOR),
                Arguments.arguments(STUDENTS, SLYTHERIN, STUDENTS_IN_SLYTHERIN),
                Arguments.arguments(STUDENTS, null, new ArrayList<Student>()),
                Arguments.arguments(STUDENTS, " ", new ArrayList<Student>()),
                Arguments.arguments(STUDENTS, "Winx", new ArrayList<Student>()),
                Arguments.arguments(null, GRYFFINDOR, null),
        };
    }

    @MethodSource("getStudentsFacultyTestArgs")
    @ParameterizedTest
    void getStudentsFacultyTest(List<Student> students, String faculty, List<Student> expected){
        List<Student> actual = cut.getStudentsFaculty(students, faculty);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getStudentsOnCourseTestArgs(){
        return new Arguments[]{
                Arguments.arguments(STUDENTS, GRYFFINDOR, "2", STUDENTS_GRYFFINDOR_2),
                Arguments.arguments(STUDENTS, SLYTHERIN, "2", STUDENTS_SLYTHERIN_2),
                Arguments.arguments(STUDENTS, null, "2", new ArrayList<Student>()),
                Arguments.arguments(STUDENTS, " ", "2", new ArrayList<Student>()),
                Arguments.arguments(STUDENTS, "Winx", "2", new ArrayList<Student>()),
                Arguments.arguments(STUDENTS, GRYFFINDOR, "0", new ArrayList<Student>()),
                Arguments.arguments(null, GRYFFINDOR, "2", null),
        };
    }

    @MethodSource("getStudentsOnCourseTestArgs")
    @ParameterizedTest
    void getStudentsOnCourseTest(List<Student> students, String faculty, String course, List<Student> expected){
        List<Student> actual = cut.getStudentsOnCourse(students, faculty, course);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] getStudentsYearTestArgs(){
        return new Arguments[]{
                Arguments.arguments(STUDENTS, 1970, STUDENTS_AFTER_1970),
                Arguments.arguments(null, 1970, null),
                Arguments.arguments(STUDENTS, 2005, new ArrayList<Student>()),
        };
    }

    @MethodSource("getStudentsYearTestArgs")
    @ParameterizedTest
    void getStudentsYearTest(List<Student> students, int year, List<Student> expected){
        List<Student> actual = cut.getStudentsYear(students, year);
        Assertions.assertEquals(expected, actual);
    }
    static Arguments[] getOneStudentYearTestArgs(){
        return new Arguments[]{
                Arguments.arguments(STUDENTS, 1970, STUDENT_1),
                Arguments.arguments(null, 1970, null),
                Arguments.arguments(STUDENTS, 2005, new ArrayList<Student>()),
        };
    }

    @MethodSource("getStudentsYearTestArgs")
    @ParameterizedTest
    void getOneStudentYearTest(List<Student> students, int year, List<Student> expected){
        List<Student> actual = cut.getStudentsYear(students, year);
        Assertions.assertEquals(expected, actual);
    }


    static Arguments[] getStudentsGroupLisTestArgs(){
        return new Arguments[]{
                Arguments.arguments(STUDENTS, "1B", STUDENTS_GROUP_1B_LIST),
                Arguments.arguments(null, "1B", null),
                Arguments.arguments(STUDENTS, "56U", new ArrayList<String>()),
        };
    }

    @MethodSource("getStudentsGroupLisTestArgs")
    @ParameterizedTest
    void getStudentsGroupListTest(List<Student> students, String group, List<String> expected) {
        List<String> actual = cut.getStudentsGroupList(students, group);
        Assertions.assertEquals(expected, actual);
    }

}