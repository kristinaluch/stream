package service;

import model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static testdata.service.StudentFormTestData.*;

class StudentFormTest {

    StudentForm cut = new StudentForm();

    static Arguments[] getFormatTestArgs(){
        return new Arguments[]{

                Arguments.arguments(STUDENTS, 4, STUDENTS_FORM),
                Arguments.arguments(null, 4, null),
                Arguments.arguments(STUDENTS, -4, null)

        };
    }


    @MethodSource("getFormatTestArgs")
    @ParameterizedTest
    void getFormatTest(List<Student> students, int limit, String expected){

        String actual = cut.getFormat(students, limit);
        Assertions.assertEquals(expected, actual);

    }
}