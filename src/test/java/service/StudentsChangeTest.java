package service;

import model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;
import static testdata.service.StudentsChangeTestData.*;


class StudentsChangeTest {

    StudentsChange cut = new StudentsChange();

    static Arguments[] changeFacultyStudentTestArgs(){
        return new Arguments[]{
                Arguments.arguments(STUDENTS_FOR_CHANGE_FACULTY, GRYFFINDOR, SLYTHERIN, STUDENTS_GRYFFINDOR_TO_SLYTHERIN),
                Arguments.arguments(null, GRYFFINDOR, SLYTHERIN, null),
                Arguments.arguments(STUDENTS_FOR_CHANGE_FACULTY, null, "r", new ArrayList<Student>()),
                Arguments.arguments(STUDENTS_FOR_CHANGE_FACULTY, " ", "r", new ArrayList<Student>()),
                Arguments.arguments(STUDENTS_FOR_CHANGE_FACULTY, "Winx", "r", new ArrayList<Student>()),
                Arguments.arguments(STUDENTS_FOR_CHANGE_FACULTY, SLYTHERIN, "", new ArrayList<Student>()),
                Arguments.arguments(STUDENTS_FOR_CHANGE_FACULTY, SLYTHERIN, null, new ArrayList<Student>())
        };
    }

    @MethodSource("changeFacultyStudentTestArgs")
    @ParameterizedTest
    void changeFacultyStudentTest(List<Student> students, String facultyFrom, String facultyTo, List<Student> expected){
        List<Student> actual = cut.changeFacultyStudent(students, facultyFrom, facultyTo);
        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] changeGroupStudentTestArgs(){
        return new Arguments[]{
                Arguments.arguments(STUDENTS_FOR_CHANGE_GROUP, "2A", "3B", STUDENTS_2A_TO_3B),
                Arguments.arguments(null, "2A", "3B", null),
                Arguments.arguments(STUDENTS_FOR_CHANGE_GROUP, null, "r", new ArrayList<Student>()),
                Arguments.arguments(STUDENTS_FOR_CHANGE_GROUP, " ", "r", new ArrayList<Student>()),
                Arguments.arguments(STUDENTS_FOR_CHANGE_GROUP, "Winx", "r", new ArrayList<Student>()),
                Arguments.arguments(STUDENTS_FOR_CHANGE_GROUP, "2A", "", new ArrayList<Student>()),
                Arguments.arguments(STUDENTS_FOR_CHANGE_GROUP, "3B", null, new ArrayList<Student>()),
        };
    }

    @MethodSource("changeGroupStudentTestArgs")
    @ParameterizedTest
    void changeGroupStudentTest(List<Student> students, String groupFrom, String groupTo, List<Student> expected){
        List<Student> actual = cut.changeGroupStudent(students, groupFrom, groupTo);
        Assertions.assertEquals(expected, actual);
    }

}