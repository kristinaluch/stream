package service;

import model.Student;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import static testdata.service.StudentTestData.*;

class StudentFindTest {

    StudentFind cut = new StudentFind();

    static Arguments[] findOnFacultyCountTestArgs(){
        return new Arguments[]{

                Arguments.arguments(STUDENTS, SLYTHERIN, 3),
                Arguments.arguments(STUDENTS, "Winx", 0),
                Arguments.arguments(null, SLYTHERIN, 0),

        };
    }

    @MethodSource("findOnFacultyCountTestArgs")
    @ParameterizedTest
    void findOnFacultyCountTest(List<Student> students, String faculty, int expected){
        int actual = cut.findOnFacultyCount(students, faculty);
        Assertions.assertEquals(expected, actual);
    }

}